

// Representative
// a data type that we expect to get from the api 
// and that can be passed around between components 
export interface Representative 
{
    name: string,
    party: string, 
    state: string,
    district: string,
    phone: string, 
    office: string, 
    link: string
}