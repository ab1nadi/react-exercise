import { useState } from 'react'
import reactLogo from './assets/react.svg'
import './App.css'
import WhoIsMyRepresentative from './components/whos-my-representative/whoismyrep'
function App() {
  const [count, setCount] = useState(0)

  return (
    <div className="App">
      <WhoIsMyRepresentative></WhoIsMyRepresentative>
    </div>
  )
}

export default App
