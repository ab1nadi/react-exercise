import './list.css';
import { Representative } from '../../../../representative';

// expected props 
interface ListProps {
    kind: string,
    reps: Representative[],
    OnRepClicked(rep: Representative): void,
}


// the List comp
// lists a given set of representatives
// the kind
export function List(props: ListProps,): JSX.Element{

    // generate the list 
    // and hookup the OnRepclicked funciton 
    // giing the id 
    let repList = props.reps.map((rep) =>
        <tr onClick={()=>{props.OnRepClicked(rep)}} key={Math.random()}>
            <td>{rep.name}</td>
            <td>{rep.party[0]}</td>
        </tr>
  );

  // render evertyhing 
      return(
      <div className="listContainer">
        <pre className="listTitle">List / <span className="listTitleInfo">{props.kind}</span></pre>
        <table className="listTable">
            <tbody>
                <tr>
                    <td>Name</td>
                    <td>Party</td>
                </tr>
                {repList}
            </tbody>
        </table>

      </div>
      );
}
