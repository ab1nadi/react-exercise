import './info.css';
import { Representative } from '../../../../representative';


// expected props
interface props
{
  rep: Representative | null,     // it can be a representative or it can be null
}

export function Info(props:props){

  // if props.rep isn't null 
  // we got a representative
  if(props.rep != null)
  {
    // splitting up first and last name
    let name = props.rep.name.split(' ');

    return (
    <div className="infoContainer">
        <div className="infoTitle">Info</div>
        <input readOnly value={name[0]}/>
        <input readOnly value={name[1]}/>
        <input readOnly value={props.rep.district}/>
        <input readOnly value={props.rep.phone}/>
        <input readOnly value={props.rep.office}/>
      </div>
    );
  }

  // no representative given just display 
  // some place holders 
  else 
  {
      return(
      <div className="infoContainer">
        <div className="infoTitle">Info</div>
        <input readOnly value={"Name"}/>
        <input readOnly value={"Last Name"}/>
        <input readOnly value={"District"}/>
        <input readOnly value={"Phone"}/>
        <input readOnly value={"Office"}/>
      </div>
      );
  }

  }
