import React from 'react';
import './whoismyrep.css';
import { Header } from './components/header/header';
import { List } from './components/list/list';
import { Info } from './components/info/info';
import { Representative } from '../../representative';
import { Controls } from './components/controls/controls';
import axios from 'axios';
const API = "http://localhost:3000/";


// the type expected
// for state
type  whoismyrepState = {
    listHeaderText: string, 
    repList: Representative[],
    infoRep: Representative | null,
}


// the WhoIsMyRepresentative component
// pretty much manages everything
// and displays all the components relavent
export default class WhoIsMyRepresentative extends React.Component<{}, whoismyrepState> {
  // settup the state
  state: whoismyrepState = {
    listHeaderText: "",
    repList: [],
    infoRep: null,
  };

    // getPeople does the api 
    // call to get the senators and representatives
    getPeople(repType: string, state:string)
    {

      // uses axios
      axios.get(API+repType.toLowerCase()+"/"+state).then((response)=>
      {
        // set state if successful
        let data = response.data;
        this.setState({
            listHeaderText:repType,
            repList:data.results,
        })
      }).catch(err=>console.log(err));
    }

    // populateInfo
    // populats info with a representative/senator
    populateInfo(rep: Representative)
    {
        this.setState({infoRep: rep});
    }

    // renders the whole thing
    render() {
      return(
      <div className="WhoIsMyRepresentative">
        <div className="headerCont">
          <Header></Header>
        </div>
        <div className="content">
          <div className="controls">
            <Controls getreps={(rep,st)=>this.getPeople(rep,st)}></Controls>
          </div>
          <div className="split">
            <div className="list">
              <List kind={this.state.listHeaderText} reps={this.state.repList} OnRepClicked={(rep)=>{this.populateInfo(rep)}}></List>
            </div>
            <div className="info">
              <Info rep={this.state.infoRep}></Info>
            </div>
          </div>
        </div>
      </div>
      );
    }
  }
